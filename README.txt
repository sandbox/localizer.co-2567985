-- SUMMARY --

Localizer is a simple and intuitive interface that makes localizing content a breeze.
Works forever with your existing website � all you need to do is copy and paste the code once!
Please get more information at http://www.localizer.co .

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual


-- CONFIGURATION --

* Open a Localizer account at www.localizer.co
* Configure your site settings at www.localizer.co
* Get the integartion code using the drupal Localizer module.
* Add the integration code to a body of your header`s block. 
* Access the Localizer editor by adding '?localizer' to your site name: http://YOURSITE/?localizer
* Check more details at http://www.localizer.co/c/4545034/1/tutorials.html


-- TROUBLESHOOTING --

* If you are having problem with displaying Localizer translation module please go to http://www.localizer.co or contact our support team at support@localizer.com.


-- CONTACT --

Current maintainers:
* Developer at Localizer (Localizer) - http://www.drupal.org/user/3276112

