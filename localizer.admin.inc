<?php

/**
 * @file
 * localizer admin configuration settings page.
 */

/**
 * localizer admin settings form.
 */
function localizer_admin_settings() {

  $form = drupal_get_form('localizer_add');

  return $form;
}


/**
 * Validate the localizer request form.
 */
function localizer_add_validate($form, &$form_state) {

  if (strlen($form['login']['#value']) < 5 || strlen($form['password']['#value']) < 5) {
    form_set_error('localizer_default_module', t('Please enter your localizer credentials.'));
  }
}

/**
 * Create the localizer request form.
 */
function localizer_add($form, &$form_state) {

  $form['login'] = array(
    '#title' => t('Login'),
    '#name' => 'login',
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => variable_get('localizer_user'),
  );

  $form['password'] = array(
    '#title' => t('Password'),
    '#name' => 'password',
    '#type' => 'password',
    '#size' => 50,
    '#maxlength' => 255,
  );

  $form['tb_link'] = array('#markup' => "<p>Don't have an account? <a href='http://www.localizer.co'>Signup</a></p>");
  $form['tb_note'] = array('#markup' => "<i><font color='red'>You have to complete your site configuration on <a href='http://www.localizer.co'>www.localizer.co</a> before proceeding further.</font></i>");

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Get Integration Code'));
  return $form;
}


/**
 * Sumbit the localizer integration code request.
 */
function localizer_add_submit($form, &$form_state) {

  try {
    $res = request_code($form['login']['#value'], $form['password']['#value']);
    $code = $res[0];
    $success = $res[1];
    if (!$success) {
      variable_set('localizer_user', '');
      form_set_error('localizer_default_module', $code);
    }
    else {
      variable_set('localizer_user', $form['login']['#value']);
      global $base_url;
      $msg = '';
      $msg = '<b>Success!</b><br><br>Here is your integration code: <i>' . htmlspecialchars($code) . '</i>';
      $msg = $msg . '<br><br>Please add this code to a body of your header`s block in order to enable Localizer on your site.';
      $msg = $msg . '<br><a href="https://www.drupal.org/documentation/modules/block">Click here</a> to get more information on how to use blocks.';
      $msg = $msg . '<br><br> You can access the Localizer editor by adding \'?localizer\' to your site name:';
      $msg = $msg . '<br><a href="' . $base_url . '/?localizer">' . $base_url . '/?localizer</a>';
      drupal_set_message($msg);
    }
  }
  catch (Exception $e) {
    form_set_error('localizer_default_module', 'Unable to get the integration code.<br>' . $e);
  }

  $form_state['redirect'] = 'admin/config/regional';
}

/**
 * Parse the localizer service request.
 */
function parse_http_response($string) {

  $headers = array();
  $content = '';
  $str = strtok($string, "\n");
  $h = NULL;
  while ($str !== FALSE) {
    if ($h and trim($str) === '') {
      $h = FALSE;
      continue;
    }

    if ($h !== FALSE and FALSE !== strpos($str, ':')) {
      $h = TRUE;
      list($headername, $headervalue) = explode(':', trim($str), 2);
      $headername = strtolower($headername);
      $headervalue = ltrim($headervalue);

      if (isset($headers[$headername])) {
        $headers[$headername] .= ',' . $headervalue;
      }
      else {
        $headers[$headername] = $headervalue;
      }
    }

    if ($h === FALSE) {
      $content .= $str . "\n";
    }

    $str = strtok("\n");
  }

  return array($headers, trim($content));
}

/**
 * Request the integration code from the localizer service.
 */
function request_code($email, $password)
{
	$postfields = array('Email'=>$email, 'Password'=>$password);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://secure.localizer.co/widget/getwidget');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	$result = curl_exec($ch);

	if($result === false)
		return array(curl_error($ch),False);
	
	if (stristr($result, 'Server Error'))
		return array("Error: There is a problem communicating with the Localizer service.",False);
	
	if (stristr($result, 'Error'))
		return array($result,False);
	
	return array($result,True);
}